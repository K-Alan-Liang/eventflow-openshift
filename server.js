var http=require("http");
var url=require("url");
var util=require("util");
http.createServer(function(req,resp){
	resp.writeHead(200,{"Content-Type":"text/html"});
	var params=url.parse(req.url,true).query;
	if(params.password&&params.password==process.env.PASSWORD){
		var res;
		if(params.crash)throw new Error();
		try{
			res=eval("(function(){"+params.code+"})()");
			res=util.inspect(res);
		}catch(e){
			res=e.stack;
		}
		resp.write('<textarea>'+res+'</textarea>');
	}
	resp.write('<form action="/" method="get">');
	resp.write('Password:<input name="password" value="'+(params.password||"")+'" /><br />');
	resp.write('Script:<textarea name="code">'+(params.code||'return "Hello World";')+'</textarea><br />');
	resp.write('<input value="Submit" type="submit" /></form><hr />');
	resp.end("EventFlow is in development. Alan Liang");
}).listen(8080);