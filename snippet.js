var log=require("./log")("snip");
var db=require("./db");
var vm=require("vm");
var ObjectId=require("mongodb").ObjectId;

var snippet=exports=module.exports=function(name,type,code){
	if(!name) name="Untitled snippet";
	if(!snippet.isType(type)){
		log.warn("unsupported type",type);
		type=snippet.GENERAL;
	}
	if(!code){
		throw new TypeError("no code provided");
	}
	this.name=name;
	this.type=type;
	this.code=code;
	return;
};

snippet.types=[];
snippet.addType=function(typeName,allowedApis){
	if(!typeName) return false;
	if(!allowedApis){
		log.warn(new Error("allowedApis not provided").stack);
		allowedApis=[];
	}
	if(snippet[typeName]){
		log.warn("snippet typeName",typeName,"duplicated");
		return false;
	}
	snippet.types.push({name:typeName,api:allowedApis});
	snippet[typeName]=snippet.types.length-1;
}
snippet.isType=function(type){
	if(typeof type==typeof 0&&type%1==0){
		if(type>-1&&type<snippet.types.length){
			return true;
		}
	}
	return false;
};

//todo: list native modules
snippet.nativeModules=[];
snippet.isNativeModule=function(name){
	for(var i=0;i<snippet.nativeModules.length;i++){
		if(snippet.nativeModules[i]===name){
			return true;
		}
	}
	return false;
};

snippet.prototype.require=function(name){
	if(typeof name!=typeof ""){
	/* prevent code run like this: (although this is not likely to happen)
	   var badName={};
	   badName.toString=function(){
	     doSomethingEvilInMainContext();
	   };
	   require(badName);
	*/
		throw new TypeError("required name is not string");
	}
	if(this.modules[name]){
		if(snippet.isNativeModule(name)) name="./"+name;
		return require(name);
	}else{
		var err=new Error("Module not required");
		err.code="EMODNREQ";
		throw err;
	}
};
snippet.prototype.requireTest=function(name){
	if(typeof name!=typeof ""){
		throw new TypeError("required name is not a string");
	}
	this.requiredModules[name]=true;
		if(snippet.isNativeModule(name)) name="./"+name;
		return require(name);
};

snippet.api={};
//todo: Snippet APIs
snippet.api.basic={};
var _on=function(name,type){
	if(!type) type=typeof function(){};
	return function(func){
		if(typeof func==type){
			this[name]=func;
			return;
		}else{
			return this[name];
		}
	};
};
snippet.api.basic.onInit=_on("onInit");
snippet.api.basic.onExit=_on("onExit");
snippet.api.basic.setParameters=function(params){
	var err=function(){
		throw new TypeError("api.basic.setParameters() invalid params");
	};
	var isValidType=function(type){
		if(typeof type==typeof /a/)return true;
		if(typeof type==typeof function(){}) return true;
		var validTypes=["Number","float","double","Function","Integer","int","uint","String","Object"];
	};
	var _params=[];
	if(!typeof params==typeof []) err();
	for(var i=0;i<params.length;i++){
		var param=params[i];
		if(!param.name) err();
		if(!isValidType(param.type)) err();
		if(!param.description)cparam.description="";
		param.required=param.required?true:false;
		_params.push({name:param.name,
		              type:param.type,
		              required:param.required,
		              description:param.description
		});
	}
};
snippet.api.trigger={};
snippet.api.trigger.onCheck=_on("onCheck");
snippet.api.trigger.trigger=function(event,timeout){
	if(!(timeout<snippet.maxTimeout)){
		timeout=snippet.defaultTimeout;
	}
	if(timeout<=0){
		timeout=snippet.defaultTimeout;
	}
	if(this.to){
		for(var i=0;i<this.to.length;i++){
			this.to[i].run(event,timeout);
		}
	}
	throw new Error("api.trigger.trigger called but snippet not linked");
};

snippet.prototype.getApi=function(){
	var allowedApis=snippet.types[this.type].api;
	var api={};
	for(var i=0;i<allowedApis.length;i++){
		var apiName=allowedApis[i];
		if(typeof snippet.api[apiName]==typeof function(){}){
			api[apiName]=snippet.api[apiName].bind(this);
		}else{
			for(var j in snippet.api[apiName]){
				api[apiName][j]=snippet.api[apiName][j].bind(this);
			}
		}
	}
	return api;
};

snippet.addType("GENERAL",["basic"]);
snippet.addType("TRIGGER",["basic","trigger"]);
snippet.addType("ACTION",["basic"]);
snippet.addType("FILTER",["basic"]);

snippet.maxTimeout=100000;
snippet.defaultTimeout=10000;
snippet.prototype.run=function(input,timeout){
	if(!timeout)timeout=snippet.defaultTimeout;
	var code="(function(){"+this.code+"})()";
	var api=this.getApi();
	var exports={};
	var module={exports:exports};
	var ctx=vm.createContext({input:input,api:api,module:module,exports:exports,require:this.require.bind(this)});
	var res=vm.runInContext(code,ctx,{timeout:timeout});
	this.exports=exports;
	this.result=res;
	return res;
};
snippet.prototype.runFunc=function(func,args,timeout){
	if(typeof timeout!=typeof 0) timeout=snippet.defaultTimeout;
	if(timeout>snippet.maxTimeout||timeout<0) timeout=snippet.defaultTimeout;
	var code="f(args)";
	var api=this.getApi();
	var exports=this.exports||{};
	var module={module:exports};
	var ctx=vm.createContext({f:f,args:args,api:api,module:module,exports:exports,require:this.require.bind(this)});
	return vm.runInContext(code,ctx,{timeout:timeout});
};
snippet.prototype.runTest=function(input,timeout){
	if(!timeout)timeout=snippet.defaultTimeout;
	var code="(function(){"+this.code+"})()";
	this.requiredModules={};
	var api=this.getApi();
	var exports={};
	var module={exports:exports};
	var ctx=vm.createContext({input:input,api:api,module:module,exports:exports,require:this.requireTest.bind(this)});
	var res=vm.runInContext(code,ctx,{timeout:timeout});
	this.exportsTest=exports;
	this.resultTest=res;
	var mods=this.requiredModules;
	this.requiredModules=undefined;
	return mods;
};
snippet.prototype.init=function(args){
	if(this.onInit){
		returnthis.runFunc(this.onInit,args);
	}
	return false;
};
snippet.prototype.exit=function(args){
	if(this.onExit){
		return this.runFunc(this.onExit,args);
	}
	return false;
};
snippet.prototype._next=function(snip){
	if(snip.constructor!=snippet){
		throw new TypeError("snippet._next() snip not a snippet");
	}
	if(this.to) this.to.push(snip);
	else this.to=[snip];
	if(snip.from) snip.from.push(this);
	else snip.from=[this];
	return;
};

snippet.collection=db.getCollection("snippets");
snippet.prototype.write=function(callback){
	var obj={};
	obj.name=this.name;
	obj.type=this.type;
	obj.code=this.code;
	obj.modules=this.modules;
	var cb=function(err,res){
		if(err){
			log.error(err.stack);
			if(callback) callback(err);
			return;
		}
		this.id=res.ops[0]._id.toString();
		callback(null,this.id);
	};
	return db.insert(snippet.collection,obj,cb);
};
snippet.prototype.update=function(update,callback){
	if(typeof update!=typeof {}){
		callback(new TypeError("update must be object"));
		return;
	}
	if(this.id){
		var cb=function(err,res){
			if(err){
				log.error(err.stack);
				if(callback) callback(err);
				return;
			}
			for(var i in update){
				this[i]=update[i];
			}
			callback(null,this.id);
		}.bind(this);
		var query={_id:new ObjectId(this.id)};
		return db.updateOne(snippet.collection,query,{$set:update},cb);
	}
};
snippet.prototype.delete=function(callback){
	if(this.id){ //saved into db
		var cb=function(err,res){
			if(err){
				log.error(err.stack);
				if(callback)callback(err);
				return;
			}
			if(callback)callback(null,res);
		};
		var query={_id:new ObjectId(this.id)};
		return db.deleteOne(snippet.collection,query,cb);
	}else{
		callback(null);
		return true;
	}
};

snippet.fromObject=function(obj){
	var snip=new snippet(obj.name,obj.type,obj.code);
	snip.modules=obj.modules;
	if(obj._id) snip.id=obj._id.toString();
	return snip;
};
snippet.fetchAll=function(callback){
	var cb=function(err,res){
		if(err){
			log.error(err.stack);
			callback(err);
			return;
		}
		var snips=[];
		for(var i=0;i<res.length;i++){
			snips.push(snippet.fromObject(res[i]));
		}
		callback(null,snips);
	};
	return db.find(snippet.collection,cb);
};
snippet.fromId=function(id,callback){
	var cb=function(err,res){
		if(err){
			log.error(err.stack);
			callback(err);
			return;
		}
		var snip=snippet.fromObject(res[0]);
		callback(null,snip);
	};
	var query={_id:new ObjectId(id)};
	return db.find(snippet.collection,query,cb);
};