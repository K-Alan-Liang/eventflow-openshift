EventFlow
=========
![Code Style: Really Bad](https://alan.liangcn.ml/CodeStyle.svg)

EventFlow: a workflow manager using [node.js](https://www.nodejs.org) .

## Development
Eventflow is in development. See below for details.

- ☐: not in development
- ✓: in development
- ✔: finished


- ☐ server.js
- ✔ log.js
- ✓ db.js
- ✓ snippet.js
- ✓ flow.js
- ☐ (other)

## Warning
This software is for testing purposes only. It has critical security issues. DO NOT tell the password to ANYONE that you do not ULTIMATELY trust. Do not use this software if you know nothing about JavaScript or node.js. Do not use any encrypted or [JSFuck](http://www.jsfuck.com)ed code.
